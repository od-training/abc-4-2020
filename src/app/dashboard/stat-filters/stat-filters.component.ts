import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css'],
})
export class StatFiltersComponent {
  filtersForm: FormGroup;

  regionOptions = [
    { label: 'Central', value: 'central' },
    { label: 'Western', value: 'western' },
    { label: 'Eastern', value: 'eastern' },
  ];

  constructor(fb: FormBuilder) {
    this.filtersForm = fb.group({
      title: [''],
      author: [''],
      region: [''],
    });

    this.filtersForm
      .get('title')
      ?.valueChanges.subscribe((value) => console.log(value));
  }
}
