import { Component, Input } from '@angular/core';
import { Video } from 'src/app/types';

@Component({
  selector: 'app-video-thumbnail',
  templateUrl: './video-thumbnail.component.html',
  styleUrls: ['./video-thumbnail.component.css'],
})
export class VideoThumbnailComponent {
  @Input('videoDetails') video?: Video;
}
