import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { Video } from '../../types';
import { VideoDataService } from 'src/app/video-data.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css'],
})
export class VideoDashboardComponent {
  videoList: Observable<Video[]>;
  selectedVideo: Observable<Video | null>;

  constructor(
    private videoDataService: VideoDataService,
    route: ActivatedRoute
  ) {
    this.videoList = videoDataService.loadVideos();

    this.selectedVideo = route.queryParamMap.pipe(
      map((params) => params.get('selectedVideo')),
      switchMap((videoId) => {
        if (videoId) {
          return this.videoDataService.getVideo(videoId);
        } else {
          return of(null);
        }
      })
    );
  }
}
