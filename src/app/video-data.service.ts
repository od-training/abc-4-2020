import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Video } from './types';

const apiUrl = 'https://api.angularbootcamp.com';

@Injectable({
  providedIn: 'root',
})
export class VideoDataService {
  constructor(private http: HttpClient) {}

  loadVideos() {
    return this.http.get<Video[]>(apiUrl + '/videos');
  }

  getVideo(id: string) {
    return this.http.get<Video>(apiUrl + '/videos/' + id);
  }
}
